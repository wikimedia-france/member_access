#!/usr/bin/env python3

import ldap
import ldap.modlist as modlist
import ldap.filter as ldfilter
from easylogger import easylogger
from logging import INFO


class WikimediaLdap:
    def __init__(self, config):
        self.ldap_host = config["ldap_host"]
        self.ldap_user = config["ldap_user"]
        self.ldap_password = config["ldap_password"]
        self.ldap_root = config["ldap_root"]

        self.logfile = config["log_file"]
        self.logger = easylogger(self.logfile, INFO)

        self.c = ldap.initialize(self.ldap_host)
        self.c.set_option(ldap.OPT_REFERRALS, 0)
        self.c.simple_bind_s(self.ldap_user, self.ldap_password)

    def getFullTree(self, root=""):
        if root == "":
            root = self.ldap_root
        return self.c.search_s(root, ldap.SCOPE_SUBTREE)

    def getPeople(self, root=""):
        if root == "":
            root = self.ldap_root
        return self.c.search_s(
            root, ldap.SCOPE_SUBTREE, filterstr="(objectClass=inetOrgPerson)"
        )

    def getGroups(self, root=""):
        if root == "":
            root = self.ldap_root
        return self.c.search_s(
            root, ldap.SCOPE_SUBTREE, filterstr="(objectClass=groupOfUniqueNames)"
        )

    def getGroupMembers(self, searchTerm="", root=""):
        if root == "":
            root = self.ldap_root
        params = [root, ldap.SCOPE_SUBTREE]
        if searchTerm:
            params.append(searchTerm)

        results = self.c.search_s(*params)
        return results[0][1]["uniqueMember"]

    def getUserGroups(self, entryId, root=""):
        if root == "":
            root = self.ldap_root
        results = self.c.search_s(
            root, ldap.SCOPE_SUBTREE, filterstr="(|(uniquemember={})(objectClass=organizationalUnit))".format(entryId)
        )

        groups = []
        for r in results:
            if 'uniqueMember' in r[1]:
                groups.append(r[0])
        return groups

    def removeUserFromGroup(self, groupId, userId):
        modlist = [(ldap.MOD_DELETE, 'uniqueMember', userId)]
        self.logger.info("Removing user {} from group {} ".format(userId, groupId))
        self.c.modify_s(groupId, modlist)

    def getEntryDetail(self, searchTerm, root=""):
        searchTerm = ldfilter.escape_filter_chars(searchTerm)
        if root == "":
            root = self.ldap_root
        params = [root, ldap.SCOPE_SUBTREE, searchTerm]
        results = self.c.search_s(*params)
        if len(results):
            return results[0]
        else:
            return None

    def deleteEntry(self, entryId):
        self.logger.info("Removing entry {}".format(entryId))
        self.c.delete_s(entryId)

    def modifyEntry(self, entryId, old, new):
        self.logger.debug("Modifing entry {}, replacing {} with {}".format(entryId, old, new))
        ldif = modlist.modifyModlist(old, new)
        self.c.modify_s(entryId, ldif)

    def replaceCnWithSn(self, entryId, newcn=""):
        # One-shot function to remove the real names
        # from the ldap directory
        entry = self.getEntryDetail(entryId)
        entryDn = entry[0]
        oldcn = entry[1]["cn"]
        if newcn == "":
            newcn = entry[1]["sn"]
        self.modifyEntry(entryDn, {"cn": oldcn}, {"cn": newcn})
        return None

    def close(self):
        self.c.unbind_s()
