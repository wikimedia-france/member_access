#!/usr/bin/env python3

import os
import errno
import configparser
from member_access_lib import *
from wikimedia_ldap_lib import *
from datetime import datetime
from pprint import pprint, pformat


def exportToFile(content, filename):
    content = pformat(content)
    export_path = 'outputs'
    # Create the directory if missing
    try:
        os.makedirs(export_path)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    with open(os.path.join(export_path, filename), 'w') as output_file:
        output_file.write(content)


if __name__ == "__main__":
    # Get the server config
    configfile = configparser.ConfigParser()
    configfile.read(os.path.join(os.path.dirname(__file__), "config.ini"))
    config = {}
    # LDAP
    config["ldap_host"] = configfile.get("ldap", "host")
    config["ldap_user"] = configfile.get("ldap", "user")
    config["ldap_password"] = configfile.get("ldap", "password")
    config["ldap_root"] = configfile.get("ldap", "root")

    # CiviCRM
    config["api_key"] = configfile.get("civicrm", "api_key")
    config["key"] = configfile.get("civicrm", "key")
    config["rest_url"] = configfile.get("civicrm", "rest_url")

    config["log_file"] = "ldap_manage.log"

    lm = WikimediaLdap(config)

    groups = lm.getGroups(root='dc=wikimedia,dc=fr')
    pprint(groups)
    #lm.getGroupMembers(root="adhwm")

    # Manually remove some groups

    """
    lm.deleteEntry('ou=systeme,ou=test,dc=wikimedia,dc=fr')
    lm.deleteEntry('ou=groupes,ou=test,dc=wikimedia,dc=fr')
    lm.deleteEntry('ou=adherents,ou=test,dc=wikimedia,dc=fr')
    lm.deleteEntry('uid=listes.wikimedia.fr-test,ou=listes,dc=wikimedia,dc=fr')
    lm.deleteEntry('cn=ecrivain,ou=systeme,ou=test,dc=wikimedia,dc=fr')
    lm.deleteEntry('cn=lecteur,ou=systeme,ou=test,dc=wikimedia,dc=fr')
    lm.deleteEntry('cn=null,ou=systeme,ou=test,dc=wikimedia,dc=fr')
    lm.deleteEntry('ou=adherentsbase,ou=groupes,dc=wikimedia,dc=fr')
    lm.deleteEntry('uid=listes.wikimedia.fr-test-editor,ou=listes,dc=wikimedia,dc=fr')
    lm.deleteEntry('uid=listes.wikimedia.fr-test-request,ou=listes,dc=wikimedia,dc=fr')
    lm.deleteEntry('uid=listes.wikimedia.fr-test-unsubscribe,ou=listes,dc=wikimedia,dc=fr')
    lm.deleteEntry('ou=test,ou=groupes,dc=wikimedia,dc=fr')
    lm.deleteEntry('ou=test,dc=wikimedia,dc=fr')
    # """

    """# remove test groups
    groups = lm.getGroups(root='ou=test,dc=wikimedia,dc=fr')
    for g in groups:
        group = g[0]
        lm.deleteEntry(group)
    # """

    
    """ # Remove non-existing entries from member lists
    groups = lm.getGroups(root='dc=wikimedia,dc=fr')
    for g in groups:
        group = g[0]
        print("Parsing group {}".format(group))

        members = lm.getGroupMembers(root=group)

        for m in members:
            parts = m.decode('utf8').split(',')
            m_sn = parts.pop(0)
            tree = ','.join(parts)
            if lm.getEntryDetail(m_sn, root=tree) is None:
                lm.removeUserFromGroup(group, m)
                print("Removing {}".format(m))
    # """

    """
    # Remove people from the
    # "ou=adherents,ou=test,dc=wikimedia,dc=fr" branch
    allTestPersons = lm.getPeople(root='ou=test,dc=wikimedia,dc=fr')
    for tp in allTestPersons:
        lm.deleteEntry(tp[0])
        # pprint(tp[0])

    # """

    """
    # Replace all real names with nicknames
    allPersons = lm.getPeople()
    fixedcn = 0
    for p in allPersons:
        entryCn = p[1]['cn']
        entrySn = p[1]['sn']
        if entryCn != entrySn:
            # If UID is set, we use it.
            if 'uid' in p[1]:
                entryUid = p[1]['uid'][0]
                entryId = 'uid={}'.format(entryUid.decode('UTF-8'))
            elif 'mail' in p[1]:
                entryMail = p[1]['mail'][0]
                entryId = 'mail={}'.format(entryMail.decode('UTF-8'))
            else:
                entryId = 'sn={}'.format(entrySn[0].decode('UTF-8'))
            lm.replaceCnWithSn(entryId)
            print("Fixing {} ({}/{})".format(entryId, entryCn, entrySn))
            fixedcn = fixedcn + 1

    print("{} names fixed".format(fixedcn))
    # """

    """ # Remove very old entries that don't have an email
    allPersons = lm.getPeople()
    for p in allPersons:
        pid = p[0]
        pdata = p[1]
        if 'mail' not in pdata:
            # pprint(pid)
            lm.deleteEntry(pid)
    #"""

    """
    # Check entries that don't have a uid or mailLocalAddress
    allPersons = lm.getPeople()
    for p in allPersons:
        pid = p[0]
        pdata = p[1]
        if 'mailLocalAddress' not in pdata:
            if 'uid' not in pdata:
                pprint(pdata)
    # """

    """ # Remove people that are not in Civicrm "wiki membres" group
    ma = MemberAccess(config)
    wikimembres = ma.get_contacts_in_group(14, id_type="email")
    allPersons = lm.getPeople()
    to_remove = []
    to_keep = []
    for p in allPersons:
        pid = p[0]
        pdata = p[1]
        if 'mailLocalAddress' in pdata:
            mail = pdata['mailLocalAddress'][0].decode('utf8')
        else:
            mail = pdata['uid'][0].decode('utf8')

        if "listes.wikimedia.fr" not in mail:
            if mail not in wikimembres:
                to_remove.append(mail)
                lm.deleteEntry(pid)
            else:
                to_keep.append(mail)

    exportToFile(to_remove, 'to_remove.txt')
    exportToFile(to_keep, 'to_keep.txt')

    # """
    # Export final state
    if 'dev' in lm.ldap_host:
        suffix = 'dev'
    else:
        suffix = 'prod'
    exportToFile(lm.getFullTree(), 'output.{} - {}.txt'.format(
        suffix,
        datetime.now().strftime("%Y-%m-%dT%H-%M")
    ))

    lm.close()
